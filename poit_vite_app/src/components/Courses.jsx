import { courses } from "../constants";
import styles from "../style";
import CoursesCard from "./CoursesCard.jsx";

const Courses = () => (
  <section id="achievements" className={`${styles.paddingY} ${styles.flexCenter} flex-col relative  `}>
    <div className="absolute z-[0] w-[60%] h-[60%] -right-[50%] rounded-full blue__gradient bottom-40" />

    <div className="w-full flex justify-between items-center md:flex-row flex-col sm:mb-16 mb-6 relative z-[1]">
      <h2 className={styles.heading2}>
        Онлайн курсы  <br className="sm:block hidden" /> по введению в IT
      </h2>
      <div className="w-full md:mt-0 mt-6">
        <p className={`${styles.paragraph} text-left max-w-[450px]`}>
            Вы изучите основные профессии в сфере IT: Frontend-разработчик,
            C#-разработчик, Python-разработчик, тестировщик.
            Разберётесь, где и для чего используются различные языки программирования и примените их на практике: напишете сайт и простые программы.
            Поймёте, какая профессия подходит именно вам.
        </p>
      </div>
    </div>

    <div className="flex flex-wrap sm:justify-center justify-center w-full feedback-container relative z-[1]">
      {courses.map((card) => <CoursesCard key={card.id} {...card} />)}
    </div>
  </section>
);

export default Courses;
