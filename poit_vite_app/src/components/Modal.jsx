import React from "react";
import {account, account_avatar, cross_1} from "../assets/index.js";
import styles from "../style.js";
import {Route, Routes} from "react-router-dom";
import Layout from "./Layout.jsx";
import Login from "./Login.jsx";
import Register from "./Register.jsx";
import LinkPage from "./LinkPage.jsx";
import Unauthorized from "./Unauthorized.jsx";
import RequireAuth from "./RequireAuth.jsx";
import Home from "./Home.jsx";
import Editor from "./Editor.jsx";
import Admin from "./Admin.jsx";
import Lounge from "./Lounge.jsx";
import Missing from "./Missing.jsx";

const ROLES = {
    'User': 2001,
    'Editor': 1984,
    'Admin': 5150
}
export default function Modal() {
    const [showModal, setShowModal] = React.useState(false);
    return (
        <>
            <div className={`${styles.flexStart} flex-row`}>
                <img
                    className="w-[30px] h-[32px]"
                    src={account}
                    alt="account"
                />
                <button
                    className=" text-xl text-gradient"
                    type="button"
                    onClick={() => setShowModal(true)}
                >
                    Войти
                </button>
            </div>


            {showModal ? (
                <>
                    <div
                        className="justify-center  items-center flex overflow-x-hidden overflow-y-auto fixed inset-0 z-50 outline-none focus:outline-none">
                        <div className="relative w-auto my-6 mx-auto max-w-3xl">

                            {/*content*/}
                            <div
                                className="border-0 rounded-lg shadow-lg relative flex flex-col w-full bg-black-gradient outline-none focus:outline-none">
                                {/*header*/}

                                <button
                                    className=" flex cursor-default  "
                                    type="button"
                                    onClick={() => setShowModal(false)}
                                >
                                    <img className=" w-[28px] h-[38px] ml-auto cursor-pointer pr-2"
                                         src={cross_1}
                                    />
                                </button>
                                <img
                                    className="w-[120px] h-[132px] m-auto"
                                    src={account_avatar}
                                    alt="avatar"

                                />
                                {/*body*/}
                                <div className="relative p-6 flex-auto">
                                    <p className="my-4 text-slate-500 text-lg leading-relaxed">
                                        <div className="max-w-md w-full space-y-8">

                                            <Routes>
                                                <Route path="/" element={<Layout/>}>
                                                    {/* public routes */}
                                                    <Route path="login" element={<Login/>}/>
                                                    <Route path="register" element={<Register/>}/>
                                                    <Route path="linkpage" element={<LinkPage/>}/>
                                                    <Route path="unauthorized" element={<Unauthorized/>}/>

                                                    {/* we want to protect these routes */}
                                                    <Route element={<RequireAuth allowedRoles={[ROLES.User]}/>}>
                                                        <Route path="/" element={<Home/>}/>
                                                    </Route>

                                                    <Route element={<RequireAuth allowedRoles={[ROLES.Editor]}/>}>
                                                        <Route path="editor" element={<Editor/>}/>
                                                    </Route>


                                                    <Route element={<RequireAuth allowedRoles={[ROLES.Admin]}/>}>
                                                        <Route path="admin" element={<Admin/>}/>
                                                    </Route>

                                                    <Route element={<RequireAuth
                                                        allowedRoles={[ROLES.Editor, ROLES.Admin]}/>}>
                                                        <Route path="lounge" element={<Lounge/>}/>
                                                    </Route>

                                                    {/* catch all */}
                                                    <Route path="*" element={<Missing/>}/>
                                                </Route>
                                            </Routes>
                                        </div>
                                    </p>
                                </div>
                                {/*footer*/}

                            </div>
                        </div>
                    </div>
                    <div className="opacity-25 fixed inset-0 z-40 bg-black"></div>
                </>
            ) : null}
        </>
    );
}