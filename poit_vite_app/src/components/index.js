import Navbar from "./Navbar";
import About from "./About";
import Stats from "./Stats";
import Courses from "./Courses";
import Pricing from "./Pricing";
import Footer from "./Footer";



export {
    Navbar,
    About,
    Stats,
    Courses,
    Pricing,
    Footer,

};
