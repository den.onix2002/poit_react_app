import React from 'react';

import {checkIcon} from "../assets";
import styles from "../style.js";

const Pricing = () => {
    return (
        <section id="enrollee" className={`flex md:flex-row flex-col ${styles.paddingY}`}>

            <div name='pricing' className='w-full text-white  '>

                <div className='max-w-[1240px] mx-auto py-12'>

                    <div className='text-center py-8 text-slate-300'>
                        <h2 className='text-3xl uppercase'>Форма обучения</h2>
                        <h3 className='text-5xl font-bold text-white py-8'>Lorem ipsum dolor sit amet.</h3>
                        <p className='text-3xl'>
                            Lorem ipsum dolor, sit amet consectetur adipisicing elit. Quia
                            laudantium odio ullam inventore aliquid ipsum quasi tenetur velit
                            voluptatum iste.
                        </p>
                    </div>

                    <div className='grid md:grid-cols-2'>

                        <div className='bg-white text-slate-900 m-4 p-8 rounded-xl shadow-2xl relative'>
                            <span
                                className='uppercase px-3 py-1 bg-indigo-200 text-indigo-900 rounded-2xl text-sm'>Бюджетная</span>
                            <div>
                                <p className='text-6xl font-bold py-4 flex'>$0<span
                                    className='text-xl text-slate-500 flex flex-col justify-end'>/год</span></p>
                            </div>
                            <p className='text-2xl py-8 text-slate-500'>Lorem ipsum dolor, sit amet consectetur
                                adipisicing.</p>
                            <div className='text-2xl'>
                                <p className='flex py-4'><img className="w-8 mr-5 text-green-600" src={checkIcon}
                                                              alt=""/>Lorem, ipsum dolor.</p>
                                <p className='flex py-4'><img className="w-8 mr-5 text-green-600" src={checkIcon}
                                                              alt=""/>Lorem, ipsum dolor.</p>
                                <p className='flex py-4'><img className="w-8 mr-5 text-green-600" src={checkIcon}
                                                              alt=""/>Lorem, ipsum dolor.</p>
                                <p className='flex py-4'><img className="w-8 mr-5 text-green-600" src={checkIcon}
                                                              alt=""/>Lorem, ipsum dolor.</p>
                                <p className='flex py-4'><img className="w-8 mr-5 text-green-600" src={checkIcon}
                                                              alt=""/>Lorem, ipsum dolor.</p>

                                <button className='w-full py-4 my-4'>Get Started</button>
                            </div>
                        </div>
                        <div className='bg-white text-slate-900 m-4 p-8 rounded-xl shadow-2xl relative'>
                            <span
                                className='uppercase px-3 py-1 bg-indigo-200 text-indigo-900 rounded-2xl text-sm'>Платная</span>
                            <div>
                                <p className='text-6xl font-bold py-4 flex'>$999<span
                                    className='text-xl text-slate-500 flex flex-col justify-end'>/год</span></p>
                            </div>
                            <p className='text-2xl py-8 text-slate-500'>Lorem ipsum dolor, sit amet consectetur
                                adipisicing.</p>
                            <div className='text-2xl'>
                                <p className='flex py-4'><img className="w-8 mr-5 text-green-600" src={checkIcon}
                                                              alt=""/>Lorem, ipsum dolor.</p>
                                <p className='flex py-4'><img className="w-8 mr-5 text-green-600" src={checkIcon}
                                                              alt=""/>Lorem, ipsum dolor.</p>
                                <p className='flex py-4'><img className="w-8 mr-5 text-green-600" src={checkIcon}
                                                              alt=""/>Lorem, ipsum dolor.</p>
                                <p className='flex py-4'><img className="w-8 mr-5 text-green-600" src={checkIcon}
                                                              alt=""/>Lorem, ipsum dolor.</p>
                                <p className='flex py-4'><img className="w-8 mr-5 text-green-600" src={checkIcon}
                                                              alt=""/>Lorem, ipsum dolor.</p>
                                <button className='w-full py-4 my-4'>Get Started</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    );
};

export default Pricing;