import menu from "./menu.svg";
import close from "./close.svg";
import account from "./account.svg"
import account_avatar from "./account-avatar.svg"
import cross_1 from "./cross_1.svg"

import dashboard from "./new-dashboard.png";
import arrowUp from "./arrow-up.svg";
import discount from "./Discount.svg";

import course_1 from "./course-1.png"
import course_2 from "./course-2.png"
import course_3 from "./course-3.png"
import course_4 from "./course-4.png"



import quotes from "./quotes.svg";

import facebook from "./facebook.svg";
import instagram from "./instagram.svg";
import linkedin from "./linkedin.svg";
import twitter from "./twitter.svg";

import checkIcon from "./check-icon.png"



export {
    menu,
    close,
    account,
    account_avatar,
    cross_1,

    dashboard,
    arrowUp,
    discount,

    course_1,
    course_2,
    course_3,
    course_4,


    quotes,

    facebook,
    instagram,
    linkedin,
    twitter,

    checkIcon,


};
