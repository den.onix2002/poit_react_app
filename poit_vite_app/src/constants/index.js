import {
  facebook,
  instagram,
  linkedin,
  twitter,
  course_1, course_2, course_3, course_4
} from "../assets";

export const navLinks = [
  {
    id: "about",
    title: "О нас",
  },
  {
    id: "achievements",
    title: "Преимущества",
  },
  {
    id: "student",
    title: "Студенту",
  },
  {
    id: "enrollee",
    title: "Абитуриенту",
  },
];



export const courses = [
  {
    id: "course-1",
    content:
      "Курсы Front End с нуля позволяют начинающим разработчикам создавать интерфейсы, с которыми взаимодействуют пользователи.",
    img: course_1,
  },
  {
    id: "course-2",
    content:
      "Язык программирования Python – один из лучших для обучения с нуля. Простой синтаксис и высокая скорость разработки – все, что нужно для разработки приложений.",
    img: course_2,
  },
  {
    id: "course-3",
    content:
      "Automation QA Engineer разрабатывает автоматические тесты для нахождения уязвимостей и проверки ПО.",
    img: course_3,
  },
  {
    id: "course-4",
    content:
        "ASP.NET разработчик создаёт приложения и игры на языке программирования C# на платформе .NET, которую поддерживает Microsoft.",
    img: course_4,
  },
];

export const stats = [
  {
    id: "stats-1",
    title: "IT-дисципллин",
    value: "48",
  },
  {
    id: "stats-2",
    title: "Специальностей",
    value: "14",
  },
  {
    id: "stats-3",
    title: "Направлений подготовки",
    value: "9+",
  },
];

export const footerLinks = [
  {
    title: "Полезные ссылки",
    links: [
      {
        name: "Moodle",
        link: "http://moodle.bru.by/",
      },
      {
        name: "Узнай свой рейтинг",
        link: "http://vuz2.bru.by/rate/",
      },
      {
        name: "Курсы",
        link: "https://www.hoobank.com/create/",
      },
      {
        name: "Расположение корпусов",
        link: "https://www.hoobank.com/explore/",
      },
      {
        name: "Контактная информация",
        link: "https://www.hoobank.com/terms-and-services/",
      },
    ],
  },
  {
    title: "Студенту",
    links: [
      {
        name: "Портфолио",
        link: "https://www.hoobank.com/help-center/",
      },
      {
        name: "Инструментарий",
        link: "https://www.hoobank.com/partners/",
      },
      {
        name: "Календарь задач",
        link: "https://www.hoobank.com/suggestions/",
      },
      {
        name: "Доска студента",
        link: "https://www.hoobank.com/blog/",
      },
      {
        name: "Методические рекомендации",
        link: "https://www.hoobank.com/newsletters/",
      },
    ],
  },
  {
    title: "Преподавателю",
    links: [
      {
        name: "Занятость",
        link: "https://www.hoobank.com/our-partner/",
      },
      {
        name: "Расписание",
        link: "https://www.hoobank.com/become-a-partner/",
      },
    ],
  },
];

export const socialMedia = [
  {
    id: "social-media-1",
    icon: instagram,
    link: "https://www.instagram.com/",
  },
  {
    id: "social-media-2",
    icon: facebook,
    link: "https://www.facebook.com/",
  },
  {
    id: "social-media-3",
    icon: twitter,
    link: "https://www.twitter.com/",
  },
  {
    id: "social-media-4",
    icon: linkedin,
    link: "https://www.linkedin.com/",
  },
];



